<?php

include_once($argv[1] . "/Animal.php");

$isidore = new Animal("Isidore", 4, Animal::MAMMAL);
$willy = new Animal("Willy", 0, Animal::FISH);
$titi = new Animal("Titi", 2, Animal::BIRD);

echo $isidore->getName() . " has " . $isidore->getLegs() . " legs and is a " . $isidore->getType() . ".\n";
echo $willy->getName() . " has " . $willy->getLegs() . " leg and is a " . $willy->getType() . ".\n";
echo $titi->getName() . " has " . $titi->getLegs() . " legs and is a " . $titi->getType() . ".\n";

$billy = new Animal("Billy", 4, Animal::MAMMAL);

echo "[Return Value Check] : There are " . Animal::getNumberOfAnimalsAlive() . " animals alive.\n";
echo "[Return Value Check] : There are " . Animal::getNumberOfMammals() . " mammals alive.\n";
echo "[Return Value Check] : There is " . Animal::getNumberOfFishes() . " fish alive.\n";
echo "[Return Value Check] : There is " . Animal::getNumberOfBirds() . " bird alive.\n";

unset($willy);
echo "[Return Value Check] : There is " . Animal::getNumberOfFishes() . " fish alive.\n";
unset($titi);
echo "[Return Value Check] : There is " . Animal::getNumberOfBirds() . " bird alive.\n";
unset($isidore);
echo "[Return Value Check] : There is " . Animal::getNumberOfMammals() . " mammal alive.\n";
unset($billy);
echo "[Return Value Check] : There is " . Animal::getNumberOfAnimalsAlive() . " animal alive.\n";