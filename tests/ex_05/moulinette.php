<?php

include_once($argv[1] . "/Cat.php");
include_once($argv[1] . "/Shark.php");
include_once($argv[1] . "/Canary.php");

$isidore = new Cat("Isidore", "orange");
$willy = new Shark("Willy");
$titi = new Canary("Titi");
$billy = new Cat("Billy");
$rafiki = new Animal("Rafiki", 4, Animal::MAMMAL);

$willy->eat($willy);
$willy->eat("");
$willy->eat([]);

$willy->eat($rafiki);
$willy->smellBlood(true);
$willy->eat($titi);
echo "[Check value changed ] ==> ";
$willy->status();
