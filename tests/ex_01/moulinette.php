<?php

include_once($argv[1] . "/Animal.php");

$isidore = new Animal("Isidore", 4, Animal::MAMMAL);
$willy = new Animal("Willy", 0, Animal::FISH);
$titi = new Animal("Titi", 2, Animal::BIRD);

echo $isidore->getName() . " has " . $isidore->getLegs() . " legs and is a " . $isidore->getType() . ".\n";
echo $willy->getName() . " has " . $willy->getLegs() . " leg and is a " . $willy->getType() . ".\n";
echo $titi->getName() . " has " . $titi->getLegs() . " legs and is a " . $titi->getType() . ".\n";