<?php

include_once($argv[1] . "/Cat.php");
include_once($argv[1] . "/Shark.php");
include_once($argv[1] . "/Canary.php");

$isidore = new Cat("Isidore", "orange");
$willy = new Shark("Willy");
$titi = new Canary("Titi");
$billy = new Cat("Billy");

echo "There are " . Animal::getNumberOfAnimalsAlive() . " animals alive.\n";

$isidore->meow();
$billy->meow();

$billy->setColor("purple");

echo $willy->getName() . " is a " . $willy->getType() . " with " . $willy->getLegs() . " leg.\n";

echo $willy->getName() . " status => ";
$willy->status();
$willy->smellBlood(true);
echo $willy->getName() . " status after changing frenzy => ";
$willy->status();

echo $titi->getName() . " has laid " . $titi->getEggsCount() . " egg.\n";
echo $titi->getName() . " is a " . $titi->getType() . " with " . $titi->getLegs() . " legs.\n";
$titi->layEgg();
echo $titi->getName() . " has laid " . $titi->getEggsCount() . " eggs.\n";
