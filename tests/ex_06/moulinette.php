<?php

include_once($argv[1] . "/Cat.php");
include_once($argv[1] . "/GreatWhite.php");
include_once($argv[1] . "/BlueShark.php");
include_once($argv[1] . "/Canary.php");

$isidore = new Cat("Isidore", "orange");
$willy = new GreatWhite("Willy");
$gonzales = new BlueShark("Gonzales");
$titi = new Canary("Titi");
$billy = new Cat("Billy");
$rafiki = new Animal("Rafiki", 4, Animal::MAMMAL);
$nemo = new Animal("Nemo", 0, Animal::FISH);


$gonzales->eat($gonzales);
$gonzales->eat("");
$gonzales->eat([]);

$gonzales->eat($rafiki);
$gonzales->smellBlood(true);
$gonzales->eat($nemo);
echo "[Check value changed ] ==> ";
$gonzales->status();


$willy->eat($willy);
$willy->eat("");
$willy->eat([]);

$willy->eat($titi);
$willy->smellBlood(true);
$willy->eat($rafiki);
echo "[Check value changed ] ==> ";
$willy->status();
$willy->eat($gonzales);
