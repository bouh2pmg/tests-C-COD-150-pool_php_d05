<?php

include_once($argv[1] . "/Cat.php");

$isidore = new Cat("Isidore", "orange");
$willy = new Animal("Willy", 0, Animal::FISH);
$titi = new Animal("Titi", 2, Animal::BIRD);
$billy = new Cat("Billy");

echo "There are " . Animal::getNumberOfAnimalsAlive() . " animals alive.\n";

$isidore->meow();
$billy->meow();

$billy->setColor("purple");

echo $billy->getName() . " is a " . $billy->getColor() . " " . $billy->getType() . " with " . $billy->getLegs() . " legs.\n";
